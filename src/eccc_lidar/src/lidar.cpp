#include "eccc_lidar/lidar.h"

typedef pcl::PointCloud<pcl::PointXYZ> PointCloud;

void callback(const PointCloud::ConstPtr& msg) {
  printf("Cloud: width = %d, height = %d\n", msg->width, msg->height);
}

int main(int argc, char** argv) {
  ros::init(argc, argv, "lidar_sub");
  ros::NodeHandle nh;
  ros::Subscriber sub = nh.subscribe<PointCloud>("/camera/depth/color/points", 1, callback);
  ros::spin();
}
